import 'package:flutter_test/flutter_test.dart';
import 'package:precept_back4app_backend/backend/back4app/schema/upload.dart';
import 'package:precept_poc/precept/pSchema.dart';

void main() {
  group('Unit test', () {
    setUpAll(() {});

    tearDownAll(() {});

    setUp(() {});

    tearDown(() {});

    test('create schema', () async {
      // given
      final appId = 'wuB4b53TtYTRc5VVm9hIoWgOpayYA0ZoTCVo0XUs';
      final masterKey = 'M5tQrGwDjpwpQfQFWihaOBvIAVF2RlFuNcv9OO3G';
      final headers = {
        "X-Parse-Application-Id": appId,
        'X-Parse-Master-Key': masterKey,
      };
      Back4AppServerSchemaHandler handler = Back4AppServerSchemaHandler();
      issueSchema.init();
      // when
      final result = await handler.createServerSchema(
          preceptSchema: issueSchema,
          documentName: 'Issue',
          headers: headers,
          version: 1);
      // then

      expect(result, true);
    });

    test('update schema', () async {
      // given
      final appId = 'wuB4b53TtYTRc5VVm9hIoWgOpayYA0ZoTCVo0XUs';
      final masterKey = 'M5tQrGwDjpwpQfQFWihaOBvIAVF2RlFuNcv9OO3G';
      final headers = {
        "X-Parse-Application-Id": appId,
        'X-Parse-Master-Key': masterKey,
      };
      Back4AppServerSchemaHandler handler = Back4AppServerSchemaHandler();
      issueSchema.init();
      // when
      final result = await handler.updateServerSchema(
          preceptSchema: issueSchema,
          documentName: 'Issue',
          headers: headers,
          version: 1);
      // then

      expect(result, true);
    });
  });
}

final clpChange = {
  'classLevelPermissions': {
    'find': {}
  }
};
